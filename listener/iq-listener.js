
'use strict';
 
const express = require('express');
const bodyParser = require('body-parser');
const { spawn } = require('child_process');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

const server = app.listen(3007, () => console.log('[iq-listener] listening on port 3007'));

app.post('/', (req, res) => {
    const payload = req.body
    console.log(payload);
});



