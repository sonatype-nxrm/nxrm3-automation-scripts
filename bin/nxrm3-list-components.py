import csv
import sys
import requests
import datetime as dt
import time

host = sys.argv[1]
user = sys.argv[2]
passwd = sys.argv[3]
repository = sys.argv[4]

base_url = 'service/rest'
components_endpoint = 'v1/components'
csvfile = "list-of-components.csv"

def get_data(query):

    req = requests.get(query, auth=(user, passwd), verify=False)

    if req.status_code == 200:
        res = req.json()
    else:
        res = "Error fetching data"

    return res


def get_components():
    page = 1
    page_size = 250

    query = "{}/{}/{}?repository={}" . format(host, base_url, components_endpoint, repository)
    continuationToken, results = page_query(query, page, page_size)

    if continuationToken is not None:
        page += 1
        query = "{}/{}/{}?continuationToken={}&repository={}" . format(host, base_url, components_endpoint, continuationToken, repository)
        continuationToken, results = page_query(query, page, page_size)

    return


def page_query(query, page, page_size):
    print(query)

    data = get_data(query)

    continuationToken = data["continuationToken"]
    items = data["items"]

    list_items (items)

    return (continuationToken, items)


def list_items(items):

    with open(csvfile, 'w') as fd:
        writer = csv.writer(fd)

        line = []
        line.append("Repository")
        line.append("Component")
        line.append("Asset")
        line.append("Date Created")
        line.append("DownloadUrl")
        line.append("Size")

        writer.writerow(line)

        for item in items:

            repository = item["repository"]
            format = item["format"]
            group = item["group"]
            name = item["name"]
            version = item["version"]

            if format.lower() == "maven2":
                component_name = group + ":" + name + ":" + version
            else:
                break

            assets = item["assets"]

            for asset in assets:
                line = []

                downloadUrl = asset["downloadUrl"]
                path = asset["path"]
                lastModified = asset["lastModified"]
                lastDownloaded = asset["lastDownloaded"]
                blobCreated = format_date(asset["blobCreated"])
                fileSize = asset["fileSize"]

                if (is_new_item(blobCreated)):
                    line.append(repository)
                    line.append(component_name)
                    line.append(path)
                    line.append(blobCreated)
                    line.append(downloadUrl)
                    line.append(fileSize)

                    print(line)
                    writer.writerow(line)

        fd.close()

    return


def format_date(thedate):
    # 2022-07-22T20:46:11.464+00:00:40

    date_str = thedate.split('T')
    time_str = date_str[1].split('.')

    formatted_str = "{}.{}".format(date_str[0], time_str[0])

    return formatted_str


def get_epoch(date_str):
    # 2022-07-22.20:46:11

    ds = date_str.split('.')

    d = ds[0].split('-')
    t = ds[1].split(':')

    year = int(d[0])
    month = int(d[1])
    day = int(d[2])
    hour = int(t[0])
    min = int(t[1])
    secs = int(t[2])

    epoch = dt.datetime(year,month,day,hour,min,secs).timestamp()

    return epoch


def is_new_item(date_str):
    is_new = False

    one_day = 86400 # secs
    current_epoch = time.time()
    age = current_epoch - one_day

    component_epoch = get_epoch(date_str)

    if (component_epoch >= age):
        is_new = True

    return is_new


def main():
    components_list = get_components()
    print(csvfile)


if __name__ == '__main__':
    main()

